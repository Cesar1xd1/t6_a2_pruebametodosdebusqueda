'''
Created on 10 dic. 2020

@author: Cesar
'''
from time import time
import random

class Corrector:
    def correcion(self):
        e=1
        while e==1:
            try:
                r = int(input())
            except:
                print("Ups, solo numero enteros mayores a 0")
                e=1
            else:
                if r>0:
                    e=0
                else:
                    print("Ups, solo numeros entero mayores a 0")
                    e=1
        return r
class BusquedaBinaria:
    comparaciones = 0;
    pasadas = 0;
   
    def binario(self,array,buscado):
        inicio=0
        final=len(array)-1
        
        while inicio<= final:
            self.pasadas=self.pasadas+1
            puntero=(inicio+final)//2
            self.comparaciones+=1
            if buscado==array[puntero]:

                return 1
            elif buscado > array[puntero]:
                self.comparaciones+=1
                inicio=puntero+1
            else:
                final=puntero-1
        
        return 0

    def quicksort(self,numeros,izq,der):
        pivote = numeros[izq]
        i = izq
        j = der
        aux = 0
        while(i<j):
            while(numeros[i]<=pivote and i <j):
                i = i +1
            while(pivote < numeros[j]):
                j = j-1
            if(i<j):
                aux = numeros[i]
                numeros[i] = numeros[j]
                numeros[j] = aux
        numeros[izq] = numeros[j]
        numeros[j] = pivote
        if(izq < j-1):
            self.quicksort(numeros, izq, j-1)
        if(j+1<der):
            self.quicksort(numeros, j+1, der)
        
    def resultadosBinarios(self, vector, valorB):
        
        tInicio = time()
        if(self.binario(vector, valorB)==1):
            print("Encontrado")
        else:
            print("No se encontro")
        tFin = time()-tInicio
        print(f"Tiempo de ejecucion =  {tFin}")
        print(f"Numero de comparaciones = {self.comparaciones}")
        print(f"Numero de pasadas = {self.pasadas}")
        self.comparaciones = 0
        self.pasadas = 0
    
class FuncionHash:
    pasadas = 0
    comparaciones =0
    
    def __init__(self):
        self.table = [None] * 127

    def funcion(self, value):
        clave = 0
        for i in range(0,len(value)):
            self.pasadas = self.pasadas+1
            clave += ord(value[i])
        return clave % 127

    def Insertar(self, value):
        hash = self.funcion(value)
        if self.table[hash] is None:
            self.table[hash] = value

    def Buscar(self,value):
        hash = self.funcion(value);
        self.comparaciones = self.comparaciones+1
        if self.table[hash] is None:
            return None
        else:
            return hex(id(self.table[hash]))

    def Eliminar(self,value):
        hash = self.funcion(value);
        if self.table[hash] is None:
            print(f"El elemento {value} no existe")
        else:
            print(f" el valor {value} a sido eliminado eliminado")
            self.table[hash] is None;
    
    def mostrar(self):
        for i in range(len(self.table)):
            if(self.table[i]!=None):
                print(self.table[i] + ", ", end="")
                
                
    def resultados(self,valor,vector):
        for i in range(len(vector)):
            self.Insertar(vector[i])
        tInicio = time()
        self.Buscar(valor)
        tFin = time() - tInicio
        print(f"Tiempo de ejecucion =  {tFin}")
        print(f"Numero de comparaciones = {self.comparaciones}")
        print(f"Numero de pasadas = {self.pasadas}")
        self.pasadas = 0
        self.comparaciones = 0
        
c = Corrector()
b = BusquedaBinaria()
fh = FuncionHash()

vector = []
for i in range(int(100)):
        vector.append(random.randint(0,100))

vectorI = [] *100
vectorS = [] *100

b.quicksort(vector, 0, len(vector)-1)
for i in range(int(100)):
    vectorI.append(vector[i])
    vectorS.append(str(vector[i]))



        
  
        
x = 0;

while(x!=3):
    print("========== MENU ===========")
    print("Digite 1 para usar el metodo de Busqueda BINARIA")
    print("Digite 2 para usar el metodo de Funcion Hash")
    print("Digite 3 para ***SALIR***")
    x = c.correcion()
    if(x==1):
        print(f"Vector: {vectorI}" )
        d = False
        while(d==False):
            print("Ingrese el valor a buscar")
            valor = input()
            if(valor.isnumeric()):
                d = True
            else:
                print("Eso no es un numero")
                d = False
        p = int(valor)    
        b.resultadosBinarios(vectorI, p)
                
    elif(x==2):
        print(f"Vecto: {vectorS}")
        print("Ingresa el elemento a buscar")   
        m = input()
        fh.resultados(m, vectorS)
    elif(x==3):
        print("Gracias por jugar")
    
    
    
    





